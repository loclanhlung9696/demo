import React, { PureComponent } from "react";

import "react-image-crop/dist/ReactCrop.css";
import ReactFullscreen from "react-easyfullscreen";

class App extends PureComponent {
  state = {
    src: null,
    crop: {
      x: 130,
      y: 50,
      width: 200,
      height: 200,
      aspect: 1,
    },
  };

  render() {
    return (
      <ReactFullscreen>
        {({ ref, onRequest, onExit }) => (
          <div
            ref={ref}
            style={{ backgroundColor: "red", width: 120, height: 120 }}
          >
            <button onClick={() => onRequest()}>FullScreen</button>
            <button onClick={() => onExit()}>Screen</button>
          </div>
        )}
      </ReactFullscreen>
    );
  }
}

export default App;
